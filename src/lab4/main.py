import numpy as np
import matplotlib.pyplot as plt
import math
import time


class Car:
    def __init__(self):
        self.time_passed = 0


n = 10  # number of time steps
Tc = 5  # time for one car to pass the crossing

means = [4, 5]  # expected values - roads ac, bd
cov_ac = [[1, 0], [0, 1]]  # sigma

a, b = np.random.multivariate_normal(means, cov_ac, n).T
c, d = np.random.multivariate_normal(means, cov_ac, n).T

A, B, C, D = np.zeros(n), np.zeros(n), np.zeros(n), np.zeros(n)  # summed number of cars on each road in current time step

is_green_vertical, is_green_horizontal = False, False  # is green light on: horizontal = roads D,B / vertical = roads A,C

green_vertical = []
green_horizontal = np.zeros(n)

is_green_vertical = True
time_passed_vertical = 0
time_passed_horizontal = 0
for i in range(n):
    a_n = max(math.floor(a[i]), 0)  # number of cars on road A in current time step
    b_n = max(math.floor(b[i]), 0)  # number of cars on road B in current time step
    c_n = max(math.floor(c[i]), 0)  # number of cars on road C in current time step
    d_n = max(math.floor(d[i]), 0)  # number of cars on road D in current time step

    print(f'A: {a_n} new cars. B: {b_n} new cars. C: {c_n} new cars. D: {d_n} new cars')

    if is_green_vertical:
        print(f'A,C GREEN. Cars going')
        if a_n > 0:
            a_n -= 1
        if c_n > 0:
            c_n -= 1

    if is_green_horizontal:
        print(f'D,B GREEN. Cars going: {time_passed_horizontal} s')
        time_passed_horizontal += 1
        if time_passed_horizontal == Tc:
            b_n = 0
            d_n = 0
    else:
        time_passed_horizontal = 0

    A[i] = a_n
    B[i] = b_n
    C[i] = c_n
    D[i] = d_n

    # else:
    #     print('GREEN LIGHT VERTICAL')
    #     print(f'A,C: CARS GOING. {time_passed} s')
    #     time_passed += 1
    #     if time_passed == Tc:
    #         time_passed = 0
    #         if An > 0:
    #             An -= 1
    #         if Cn > 0:
    #             Cn -= 1
    #         print(f'CARS GONE. {time_passed} s')
    #
    # if not is_green_horizontal:
    #     Bn += b_n
    #     Dn += d_n

    print(f'road A: {A}, road B: {B}, road C: {C}, road D: {D}')
    time.sleep(5)


